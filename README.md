<div>
  
  <div align="center">
      <a href="https://gitlab.com/Flipe124">
        <img height="190px" src="https://github-readme-streak-stats.herokuapp.com/?user=flipe124&theme=midnight-purple" alt="flipe124" />
      <img height="190px" src="https://github-readme-stats.vercel.app/api/top-langs/?username=Flipe124&layout=compact&langs_count=7&theme=midnight-purple">
        
   ![snake gif](https://github.com/rafaballerini/rafaballerini/blob/output/github-contribution-grid-snake.svg)
   
  </div>    
  <div align="center">
      <img align="center" alt="Java" height="34" width="44" src="https://cdn.icon-icons.com/icons2/2108/PNG/512/java_icon_130901.png">
      <img align="center" alt="HTML" height="40" width="50" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg">
      <img align="center" alt="CSS" height="40" width="50" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original.svg">
      <img align="center" alt="Python" height="40" width="50" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg">
  </div>  
  <div align="center"> 
      <a href = "mailto:felipe.morais.job@gmail.com">
        <img src="https://img.shields.io/badge/-Gmail-%23333?style=for-the-badge&logo=gmail&logoColor=white" target="_blank">
      </a>
      <a href="https://www.linkedin.com/in/felipe-oliveira-dos-santos-morais-a016991b9/" target="_blank"><img src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank">
      </a>  
  </div>
</div>
